<?php

$host = ( !empty( $_SERVER['MYSQL_HOST'] ) )? $_SERVER['MYSQL_HOST'] : 'localhost';
$user = ( !empty( $_SERVER['MYSQL_USER'] ) )? $_SERVER['MYSQL_USER'] : 'root';
$pass = ( !empty( $_SERVER['MYSQL_PASSWORD'] ) )? $_SERVER['MYSQL_PASSWORD'] : '1234567890';
$db = ( !empty( $_SERVER['MYSQL_API_DATABASE'] ) )? $_SERVER['MYSQL_API_DATABASE'] : 'pamfax';

$socket = "mysql://$user:$pass@$host";

return [
    'dev' =>
        [
            "data"			=> "$socket/$db?new",
            "wp"			=> "$socket/{$db}_web?new",
            "ui"			=> "$socket/{$db}_ui?new",
            "internal"		=> "$socket/{$db}_internal?new",
            "shop"			=> "$socket/{$db}_shop?new",
            "download"		=> "$socket/{$db}_report?new",
            "console"		=> "$socket/{$db}_console?new",
            "affiliate"		=> "$socket/{$db}_affiliate?new",
        ]
];
