<?php

$name = ( !empty( $_SERVER['HOST_NAME'] ) )? $_SERVER['HOST_NAME'] : 'pamfax';
$domain = ( !empty( $_SERVER['HOST_DOMAIN'] ) )? $_SERVER['HOST_DOMAIN'] : 'local';
$web = "www.$name.$domain";

$dev = [ "web" => $web, "web_de" => "$web/de",
         "web_jp" => "$web/ja", "web_en" => "$web/en" ];

foreach( [ 'api', 'addons', 'download',
           'console', 'client', 'common',
           'portal', 'frontend', 'system', 'shop' ] as $service )
    $dev[ $service ] = "$service.$name.$domain";

return [ "dev" => $dev ];

