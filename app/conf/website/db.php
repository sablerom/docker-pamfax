<?php
/**
 * Created by PhpStorm.
 * User: roan
 * Date: 6.3.18
 * Time: 10.47
 */
$host = ( !empty( $_SERVER['MYSQL_HOST'] ) )? $_SERVER['MYSQL_HOST'] : 'localhost';
$user = ( !empty( $_SERVER['MYSQL_USER'] ) )? $_SERVER['MYSQL_USER'] : 'root';
$pass = ( !empty( $_SERVER['MYSQL_PASSWORD'] ) )? $_SERVER['MYSQL_PASSWORD'] : '1234567890';
$db = ( !empty( $_SERVER['MYSQL_SHOP_DATABASE'] ) )? $_SERVER['MYSQL_SHOP_DATABASE'] : 'pamfax_shop';

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host=$host;dbname=$db",
    'username' => $user,
    'password' => $pass,
    'charset' => 'utf8',

	// Enable schema cache.
	'enableSchemaCache' => YII_ENV_DEV ? false : true,

    // Duration of schema cache.
    'schemaCacheDuration' => YII_ENV_DEV ? 0 : 43200,

    // Name of the cache component used to store schema information
    'schemaCache' => YII_ENV_DEV ? null : 'cache'

];