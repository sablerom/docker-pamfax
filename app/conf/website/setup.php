<?php

$name = DeployManager::getEnv('HOST_NAME', 'pamfax' );
$domain = DeployManager::getEnv('HOST_DOMAIN', 'local');

$domainRoot = "$name.$domain";
$domainShop = "shop.$name.$domain";
$domainPortal = "new-portal.$name.$domain";
$domainServer = $_SERVER["SERVER_NAME"];

$t_scheme = empty($_SERVER['REQUEST_SCHEME']) ? 'https' : $_SERVER['REQUEST_SCHEME'];

return [
	'common' => [
		'main' => [
            'components' => [
                'request' => [
                    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                    'cookieValidationKey' => 'faCsYux9dlUlo8N4-nsvCmZg9xSSE0Eh',
                ],
                'assetManager'  => [
                    'linkAssets' => true
                ],
                'vueManager'   => [
                    'class' => 'common\components\VueManager',
                    'delimiters' => [ '[[', ']]' ],
                    'isDev' => DeployManager::checkEnv( 'DEPLOY_MODE', 'develop' )
                ],
                'cache' => [
                    /*Using Memcache-to enabled*/
//                    'class' => 'yii\caching\MemCache',
//                    'useMemcached'=>true, // false, // for BETA/LIVE
//                    'servers' => [
//                        [
//                            'host' => 'localhost', //85.13.144.91 for LIVE
//                            'port' => 11211,
//                            'weight' => 100,
//                        ]
//              ],

                    /*Using cache-to disabled*/
                    //'class'=>'yii\caching\DummyCache',
                    'class'=>'yii\caching\FileCache'

                    /*Using cache-to ApcCahe*/
                    //'class' => 'yii\caching\ApcCache',
                ]
            ],
		],
		'params' => [
            'IS_DEV'            => false,
            'IS_LIVE'			=> false,

            'LOCATION'			=> "BY", // for pamfax.biz should be "DE"

            'new_portal_login'	=> true,

            'scheme'			=> $t_scheme,

            'adminEmail1'		=> 'portal2feedback@pamfax.biz',

            'PAMFAX_API_URL'         => "https://api.pamfax.local/",
            'PAMFAX_API_APPLICATION' => "pamfax_portal",
            'PAMFAX_API_SECRET_WORD' => "Cudr8juprUGE",

            'domainRoot'		 => $domainRoot,
            'domainPortal'		 => $domainPortal,
            'domainShop'		 => $domainShop,
            'domainServer'		 => $domainServer,

            'WWW_URL'			 => $t_scheme.'://www.'.$domainRoot,
            'cookieDomain'       => $domainRoot,
            'pamfaxWebUrl'       => $t_scheme.'://'.$domainRoot,
            'pamfaxPortalUrl'    => $t_scheme.'://'.$domainPortal,
            'pamfaxDownloadUrl'  => $t_scheme.'://download.'.$domainRoot,
            'pamfaxShopUrl'		 => $t_scheme.'://'.$domainShop,
            'pamfaxServerUrl'    => $t_scheme.'://'.$domainServer,
            'allowSsl'           => 1,
            'disable_GA'         => 0,

            'outerUrl'           => [
                'sendFax'		 => $t_scheme.'://'.$domainPortal.'/fax/send/',
                'buyCredit'      => $t_scheme.'://'.$domainPortal.'/payment/buycredit/',
                'buyProPlan'     => $t_scheme.'://'.$domainShop.'/pamfax_professional_fax_plan?category_id=1',
                'buyBasicPlan'   => $t_scheme.'://'.$domainShop.'/pamfax_basic_plan?category_id=1'
            ],

            'recaptchaKey'       => '6LcC5jwUAAAAAGlPZjKYTUdNg_TJ4TWxsOjC7HLs', //'6Ld3eCATAAAAACdUwQzdfMXwXahvShya42EdWv1c';
            'recaptchaSecretKey' => '6LcC5jwUAAAAAC18aWOsal61OApDF9MGm64EAUYx', //'6Ld3eCATAAAAALJuAGhVxFl8KMyo_1PTduiiuyUY';

            'downloadLocation'   => "/vagrant/public/download/files", // "/www/htdocs/w008b779/3.0/.files/download", // for LIVE/BETA

            'downloadShortMap'   => [
                "PamFaxSetup.exe"		=> "client/latest/windows/PamFaxSetup.exe",
                "PamFaxSetup.msi"		=> "client/latest/windows/PamFaxSetup.msi",
                "PamFaxSetup-x64.exe"	=> "client/latest/windows-x64/PamFaxSetup.exe",
                "PamFaxSetup-x64.msi"	=> "client/latest/windows-x64/PamFaxSetup.msi"
            ]
		]
	],

	'backend' => [
		'main' => [
            'components' => [
                'request' => [
                    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                    'cookieValidationKey' => 'hCe2U_FUbis3YNH2taZBtTTxS9Vi4pa8'
                ]
            ]
		],
		'params' => [
            'cookieDomain' => '.'.$domainRoot,
		]
	],

	'frontend' => [
		'main' => [],
		'params' => [
				'cookieDomain'       => '.'.$domainRoot,
		]
	]
];